<?php
$root = "../";
$page['require_login'] = false;
$page['title'] = "Login";
$page['navbar'] = false;
$page['footer'] = false;
$page['css'] = array("/assets/css/login.css");
require_once $root . "app/includes/autoload.php";

// Source: https://getbootstrap.com/docs/4.5/examples/
echo '<body class="text-center">
    <form class="form-signin" method="post" action="/api/login.php">
      <img class="mb-4" src="/assets/img/login.svg" alt="" width="150" height="150">
      <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>';
if (isset($_GET['error']) && $_GET['error']) {
    echo '<div class="alert alert-danger">Username, password or 2FA code wrong.</div>';
} elseif (isset($_GET['reset']) && $_GET['reset']) {
    echo '<div class="alert alert-success">Your password has been reset!</div>';
}
echo '<label for="username" class="sr-only">Username</label>
      <input type="text" id="username" name="username" class="form-control" placeholder="Username" required autofocus>
      <label for="password" class="sr-only">Password</label>
      <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
      <label for="2fa" class="sr-only">2FA Login Code</label>
      <input type="number" id="2fa" name="2fa" class="form-control" min="100000" maxlength="999999" placeholder="2FA Login Code" required>
      <div class="checkbox mb-3">
        <label>
          <input type="checkbox" value="remember-me" name="rememberMe"> Remember me
        </label>
      </div>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      <a href="/requestPassword.php">Forgot password?</a>
      <p class="mt-5 mb-3 text-muted">&copy; 2020</p>
    </form>';


require_once $root . "app/includes/footer.php";
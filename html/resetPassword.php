<?php
$root = "../";
$page['require_login'] = false;
$page['title'] = "Login";
$page['navbar'] = false;
$page['footer'] = false;
$page['css'] = array("/assets/css/login.css");
require_once $root . "app/includes/autoload.php";

if (!isset($_GET['key'])) {
    header("Location: /requestPassword.php");
    die("Malformed request");
}
$keyResult = $database->prepare("SELECT * FROM `passwordRequests` WHERE `key` = ? AND `status` = 'VALID' AND `time` > ?;");
$keyResult->bindParam(1, $_GET['key']);
$startKeyTime = time() - 86400;
$keyResult->bindParam(2, $startKeyTime);
$keyResult->execute();
if ($keyResult->rowCount() != 1) {
    header("Location: /requestPassword.php");
    die("Unknown token");
}

// Source: https://getbootstrap.com/docs/4.5/examples/
echo '<body class="text-center">
    <form class="form-signin" method="post" action="/api/resetPassword.php">';

if (isset($_GET['error']) && $_GET['error'] == 'mismatch') {
    echo '<div class="alert alert-danger">The passwords do not match!</div>';
}

echo '<input type="hidden" name="key" value="' . $_GET['key'] . '"/>
      <img class="mb-4" src="/assets/img/login.svg" alt="" width="150" height="150">
      <h1 class="h3 mb-3 font-weight-normal">Reset your password</h1>
      <label for="password" class="sr-only">New Password</label>
      <input type="password" id="password" name="password" class="form-control" placeholder="New password" required autofocus>
      <label for="passwordConfirmation" class="sr-only">Confirm new Password</label>
      <input type="password" id="passwordConfirmation" name="passwordConfirmation" class="form-control" placeholder="Confirm new password" required autofocus>
      <br>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Reset password</button>
      <a href="/login.php">Know your password?</a>
      <p class="mt-5 mb-3 text-muted">&copy; 2020</p>
    </form>';


require_once $root . "app/includes/footer.php";
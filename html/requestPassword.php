<?php
$root = "../";
$page['require_login'] = false;
$page['title'] = "Login";
$page['navbar'] = false;
$page['footer'] = false;
$page['css'] = array("/assets/css/login.css");
require_once $root . "app/includes/autoload.php";

// Source: https://getbootstrap.com/docs/4.5/examples/
echo '<body class="text-center">
    <form class="form-signin" method="post" action="/api/requestPassword.php">
      <img class="mb-4" src="/assets/img/login.svg" alt="" width="150" height="150">
      <h1 class="h3 mb-3 font-weight-normal">Request new password</h1>';
if (isset($_GET['success']) && $_GET['success']) {
    echo '<div class="alert alert-success">If an account exist with this e-mail address, we will send you an reset link soon.</div>';
}
echo '<label for="email" class="sr-only">E-Mail Address</label>
      <input type="email" id="email" name="email" class="form-control" placeholder="E-mail" required autofocus>
      <br>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Request reset</button>
      <a href="/login.php">Know your password?</a>
      <p class="mt-5 mb-3 text-muted">&copy; 2020</p>
    </form>';


require_once $root . "app/includes/footer.php";
<?php
$root = "../../";
$page['require_login'] = false;
// headless page
$page['header'] = false;
$page['footer'] = false;
require_once $root . "app/includes/autoload.php";
require_once $root . "app/includes/lib/PostUtil.php";

if (!areFieldsSet(array("key", "password", "passwordConfirmation"))) {
    header("Location: /requestPassword.php");
    die("Bad request");
}
// check if token exist, check if user exist and is not banned, invalidate token, set password

$keyResult = $database->prepare("SELECT * FROM `passwordRequests` WHERE `key` = ? AND `status` = 'VALID' AND `time` > ?;");
$keyResult->bindParam(1, $_POST['key']);
$startKeyTime = time() - 86400;
$keyResult->bindParam(2, $startKeyTime);
$keyResult->execute();
if ($keyResult->rowCount() != 1) {
    header("Location: /requestPassword.php");
    die("Unknown token");
}
$keyResult = $keyResult->fetch(PDO::FETCH_ASSOC);
$userId = $keyResult['user'];


$userResult = $database->prepare("SELECT * FROM `users` WHERE `id` = ? AND `status` = 'ACTIVE';");
$userResult->bindParam(1, $userId);
$userResult->execute();
if ($userResult->rowCount() != 1) {
    header("Location: /requestPassword.php");
    die("User not active!");
}
$userResult = $userResult->fetch(PDO::FETCH_ASSOC);
$email = $userResult['email'];

$keyResult = $database->prepare("UPDATE `passwordRequests` SET `status` = 'INVALID' WHERE `key` = ?;");
$keyResult->bindParam(1, $_POST['key']);
$keyResult->execute();

if ($_POST['password'] != $_POST['passwordConfirmation']) {
    header("Location: /requestPassword.php?key=" . $_POST['key'] . "&error=mismatch");
    die("Passwords do not match");
}
$passwordHash = password_hash($_POST['password'], PASSWORD_BCRYPT);

$userResult = $database->prepare("UPDATE `users` SET `password` = ? WHERE `id` = ?;");
$userResult->bindParam(1, $passwordHash);
$userResult->bindParam(2, $userId);
$userResult->execute();


// send email
$header = "MIME-Version: 1.0\r\n";
$header .= "Content-type: text/plain; charset=utf-8\r\n";
$header .= "From: " . $config['email_sender_address'] . "\r\n";

$text = "Hi there!\n\n
Your password has beer reset.\n\n
If you did so, you can ignore this email.\n\n.
If not, please contact the support.\n\n
This email has been sent automatically, so don't expect an answer.";

mail($email, $config['page_title'] . " - Passwort has been reset", $text, $header);

header("Location: /login.php?reset=true");
die("success");
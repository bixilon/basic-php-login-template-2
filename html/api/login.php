<?php
$root = "../../";
$page['require_login'] = false;
// headless page
$page['header'] = false;
$page['footer'] = false;
require_once $root . "app/includes/autoload.php";
require_once $root . "app/includes/lib/PostUtil.php";

if (!areFieldsSet(array("username", "password", "2fa"))) {
    header("Location: /login.php");
    die("Bad request");
}
$loginResult = loginUser($_POST['username'], $_POST['password'], $_POST['2fa'], isset($_POST['rememberMe']) && $_POST['rememberMe'] == 'on');
if (!$loginResult) {
    header("Location: /login.php?error=true");
    die("Username, password or 2FA code wrong.");
}
header("Location: /");
die('Login okay, please go to <a href="/">/index.php</a>');
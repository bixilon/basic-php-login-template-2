<?php
$root = "../../";
$page['require_login'] = false;
// headless page
$page['header'] = false;
$page['footer'] = false;
require_once $root . "app/includes/autoload.php";
require_once $root . "app/includes/lib/PostUtil.php";

if (!areFieldsSet(array("email"))) {
    header("Location: /requestPassword.php");
    die("Bad request");
}
$userResult = $database->prepare("SELECT * FROM `users` WHERE `email` = ?;");
$userResult->bindParam(1, $_POST['email']);
$userResult->execute();
if ($userResult->rowCount() != 1) {
    header("Location: /requestPassword.php?success=true");
    die("success");
}
$userResult = $userResult->fetch(PDO::FETCH_ASSOC);
require_once $root . 'app/includes/lib/StringUtil.php';
$resetKey = generateRandomString(256);

$resetResult = $database->prepare("INSERT INTO `passwordRequests` (`key`, `user`, `time`, `status`) VALUES (?, ?, ?, 'VALID');");
$resetResult->bindParam(1, $resetKey);
$resetResult->bindParam(2, $userResult['id']);
$time = time();
$resetResult->bindParam(3, $time);
$resetResult->execute();


// send email
$header = "MIME-Version: 1.0\r\n";
$header .= "Content-type: text/plain; charset=utf-8\r\n";
$header .= "From: " . $config['email_sender_address'] . "\r\n";

$text = "Hi there!\n\n
A new password has been requested for your account.\n\n
If you did so, please open this link in your browser: https://" . $config['page_base_url'] . "/resetPassword.php?key=" . $resetKey . "\n\n.
If not, you can safely ignore this email. The link only stays valid for 1 hour.\n\n
This email has been sent automatically, so don't expect an answer.";

mail($userResult['email'], $config['page_title'] . " - Password reset", $text, $header);

header("Location: /requestPassword.php?success=true");
die("success");
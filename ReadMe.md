# Basic PhP login template with bootstrap

## Overview
This is a really basic login template, written in PhP.

## Features
- Written in PhP
- No JavaScript needed
- Beautiful design (Bootstrap)
- Login (Username, Password and 2FA)
- Password reset (via email)
- Template (Navbar, etc)

## Requirements
- Written in Php 7.4 (maybe compatible with lower versions, untested)
- PhP MySQL Addon
- MySQL Server (MariaDB compatible)
- For 2FA: The correct time on the host machine


## Installation
1. Copy the `/app/includes/config/config.inc.sample.php` to `/app/includes/config/config.inc.php` and insert all values
2. Import the database (`/database.sql`) into your MySQL server
3. Customize the files in the `/html` folder, feel free to add pages, etc


## Create a user
1. Create a password BCRYPT hash with your password
2. Optional: Create a random base32 string for 2FA.
3. Insert the data into the `users` table


## Groups
Insert the group id into the `users` table for each user. A higher number means more permission.
Now you can add all groups into the `/app/includes/user/User.php` at the bottom. You can check with
`User::hasPermission` if the user has at least the permission level for the specific group (1. parameter)


## Questions? Open an issue
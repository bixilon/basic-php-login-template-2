<?php
echo '<!doctype html><html lang="de">';

// header
echo '<head>';

echo '<meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"><link rel="icon" href="/favicon.ico">';

// title
echo '<title>' . (isset($page['title']) ? $page['title'] : "Unknown") . ' - ' . $config['page_title'] . '</title>';

// css
if (isset($page['css'])) {
    foreach ($page['css'] as $css) {
        echo '<link rel="stylesheet" href="' . $css . '">';
    }
}
echo '<link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">';

echo '</head>';


if (!isset($page['navbar']) || $page['navbar']) {
    echo '<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
  <a class="navbar-brand" href="/">' . $config['page_title'] . '</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbar">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
        <div class="dropdown-menu" aria-labelledby="dropdown01">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
    </ul>
      <div class="mr-sm-2">Hello, ' . $user->getDisplayName() . '</div>
      <a class="btn btn-outline-danger my-2 my-sm-0" href="/logout.php" ">Logout</a>
  </div>
</nav>';
}

echo '<body>';
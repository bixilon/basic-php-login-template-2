<?php
$config['database_host'] = "127.0.0.1";
$config['database_username'] = "user";
$config['database_password'] = "123";
$config['database_database'] = "password";


$config['page_title'] = "Basic Login Template v2";

$config['login_default_time'] = 86400;
$config['login_remember_me_time'] = 2629746;

$config['page_base_url'] = "https://example.com";
$config['debug'] = true;

// email
$config['email_sender_address'] = "test@test.de";

class Groups
{
    const MEMBER = 0;
    const USER = 1;
    const ADMINISTRATOR = 2;
}
<?php

function areFieldsSet($fields)
{
    global $_POST;
    foreach ($fields as $field) {
        if (!isset($_POST[$field])) {
            return false;
        }
    }
    return true;
}
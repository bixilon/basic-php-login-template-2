<?php

// footer
if (!isset($page['footer']) || $page['footer']) {
    echo '<footer class="container"><p>&copy; Moritz Zwerger 2020</p></footer>';
}

// include java script
echo '<script src="/assets/jquery/js/jquery-3.4.1.min.js"></script>';
echo '<script src="/assets/popper/js/popper.min.js"></script>';
echo '<script src="/assets/bootstrap/js/bootstrap.min.js"></script>';

echo '</body></html>';

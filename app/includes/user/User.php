<?php


class User
{
    private $loggedIn = false;
    private $id;
    private $username;
    private $email;
    private $password;
    private $twoFactorAuthenticationToken;
    private $group;
    private $displayName;

    function __construct($sessionKey)
    {
        global $database, $config;
        if (!isset($sessionKey)) {
            return;
        }
        $sessionResult = $database->prepare("SELECT * FROM `sessions` WHERE `key` = ? AND `status` = 'VALID';");
        $sessionResult->bindParam(1, $sessionKey);
        $sessionResult->execute();
        if ($sessionResult->rowCount() != 1) {
            $this->logout();
            return;
        }

        $sessionResult = $sessionResult->fetch(PDO::FETCH_ASSOC);

        if ($sessionResult['rememberMe'] == 'YES') {
            if (time() - $sessionResult['startTime'] > $config['login_remember_me_time']) {
                $this->logout();
                return;
            }
        } else {
            if (time() - $sessionResult['startTime'] > $config['login_default_time']) {
                $this->logout();
                return;
            }
        }

        $userResult = $database->prepare("SELECT * FROM `users` WHERE `id` = ? AND `status` = 'ACTIVE';");
        $userResult->bindParam(1, $sessionResult['user']);
        $userResult->execute();
        if ($userResult->rowCount() != 1) {
            $this->logout();
            return;
        }
        $userResult = $userResult->fetch(PDO::FETCH_ASSOC);

        $this->id = $userResult['id'];
        $this->username = $userResult['username'];
        $this->email = $userResult['email'];
        $this->password = $userResult['password'];
        $this->twoFactorAuthenticationToken = $userResult['2FA'];
        $this->group = $userResult['group'];
        $this->displayName = $userResult['displayName'];
        $this->loggedIn = true;
    }

    function logout()
    {
        global $database;
        $res = $database->prepare("DELETE FROM `sessions` WHERE `key` = ?;");
        $res->bindParam(1, $_COOKIE['session']);
        $res->execute();
        setcookie("session", "", 0);
    }

    /**
     * @return bool
     */
    public function isLoggedIn()
    {
        return $this->loggedIn;
    }

    /**
     * @return string
     */

    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return int
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param $group Groups
     * @return bool
     */
    public function hasPermission($group)
    {
        return $this->group >= $group;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

}

function getCurrentUser()
{
    global $user;
    if (isset($user)) {
        return $user;
    }
    $user = new User(getSessionKey());
    return $user;
}

/**
 * @return bool
 */
function loginUser($username, $password, $twoFactorAuthenticationNumber, $rememberMe)
{
    global $database, $config, $root;
    $userResult = $database->prepare("SELECT * FROM `users` WHERE `username` = ? AND `status` = 'ACTIVE';");
    $userResult->bindParam(1, $username);
    $userResult->execute();
    if ($userResult->rowCount() != 1) {
        return false;
    }
    $userResult = $userResult->fetch(PDO::FETCH_ASSOC);

    // 2fa
    if (strlen($userResult['2FA']) >= 5 && !$config['debug']) { // is 2fa enabled and page not in debug mode
        include_once($root . "app/includes/lib/2fa/GoogleAuthenticator.php");
        $auth = new GoogleAuthenticator();
        if (!$auth->checkCode($userResult['2FA'], $_POST['2fa']))
            return false;
    }

    // password
    if (!password_verify($password, $userResult['password'])) {
        return false;
    }

    global $root, $config;
    require_once $root . 'app/includes/lib/StringUtil.php';
    $sessionKey = generateRandomString(256);
    $sessionResult = $database->prepare("INSERT INTO `sessions` (`key`, `user`, `status`, `os`, `browser`, `startIp`, `rememberMe`, `startTime`) VALUES (?, ?, 'VALID', 'os', 'browser', ?, ?, ?);");
    $sessionResult->bindParam(1, $sessionKey);
    $sessionResult->bindParam(2, $userResult['id']);
    $sessionResult->bindParam(3, $_SERVER['REMOTE_ADDR']);
    $rememberMeString = $rememberMe ? 'YES' : 'NO';
    $sessionResult->bindParam(4, $rememberMeString);
    $time = time();
    $sessionResult->bindParam(5, $time);
    $sessionResult->execute();

    if ($rememberMe) {
        //expires not so fast
        setcookie("session", $sessionKey, time() + $config['login_remember_me_time'], "/");
        return true;
    }
    setcookie("session", $sessionKey, 0, "/"); //until end of session
    return true;
}
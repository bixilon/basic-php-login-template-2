<?php
if (!isset($root)) {
    $root = "../../";
}
function loadConfig()
{
    global $root;
    if (!file_exists($root . "app/includes/config/config.inc.php")) {
        header("Location: /install");
        die("config.inc.php does not exists. Please reinstall");
    }
    require_once $root . "app/includes/config/config.inc.php";
    return $config;
}

$config = loadConfig();

if (!isset($config)) {
    die("Malformed configuration!");
}


require_once $root . "app/includes/database.php";
$database = connectToMySQL($config['database_host'], $config['database_username'], $config['database_password'], $config['database_database']);

require_once $root . "app/includes/user/User.php";

$user = getCurrentUser();

function getUser()
{
    global $user;
    return $user;
}

if (isset($page)) {
    if (isset($page['require_login'])) {
        // if true, redirect (if not logged in) to login
        // if false, deny if logged in
        if ($page['require_login']) {
            if (!$user->isLoggedIn()) {
                header("Location: /login.php");
                die("You don't have the permission to access this page!");
            }
        } else {
            if ($user->isLoggedIn()) {
                header("Location: /");
                die("You are already logged in!");
            }
        }
    }
    if (!isset($page['header']) || $page['header']) {
        require_once $root . "app/includes/header.php";
    }
}
function loadHeader()
{
    global $root;
    require_once $root . "app/includes/header.php";
}

function getSessionKey()
{
    if (!isset($_COOKIE['session'])) {
        return "";
    }
    return $_COOKIE['session'];
}
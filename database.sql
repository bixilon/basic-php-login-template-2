-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Erstellungszeit: 19. Sep 2020 um 20:18
-- Server-Version: 10.5.5-MariaDB-1:10.5.5+maria~focal
-- PHP-Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `login-template-2`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `passwordRequests`
--

CREATE TABLE `passwordRequests` (
  `key` varchar(256) NOT NULL,
  `user` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `status` enum('VALID','INVALID') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `sessions`
--

CREATE TABLE `sessions` (
  `key` varchar(256) NOT NULL,
  `user` int(11) NOT NULL,
  `status` enum('VALID','INVALID') NOT NULL,
  `os` varchar(256) NOT NULL,
  `browser` varchar(256) NOT NULL,
  `startIp` varchar(256) NOT NULL,
  `rememberMe` enum('YES','NO') NOT NULL,
  `startTime` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `password` text NOT NULL,
  `2FA` varchar(256) NOT NULL,
  `creation` int(11) NOT NULL,
  `group` int(11) NOT NULL,
  `displayName` text NOT NULL,
  `status` enum('ACTIVE','INACTIVE','WAITING_ACTIVATION','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `passwordRequests`
--
ALTER TABLE `passwordRequests`
  ADD UNIQUE KEY `key` (`key`);

--
-- Indizes für die Tabelle `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `key` (`key`);

--
-- Indizes für die Tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
